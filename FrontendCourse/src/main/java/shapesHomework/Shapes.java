package shapesHomework;

public class Shapes {
    public static void drawShapeOutline(int m, int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (i == 1 || i == n) {
                    System.out.print("*");
                } else if (j == 1 || j == m) {
                    System.out.print("*");
                } else System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static void drawShapeCorners(int n, int m) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (i == 1 && j == 1 || i == 1 && j == m || i == n && j == 1 || i == n && j == m) {
                    System.out.print("*");
                } else System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        drawShapeCorners(4, 7);
        drawShapeOutline(3, 8);
    }
}
