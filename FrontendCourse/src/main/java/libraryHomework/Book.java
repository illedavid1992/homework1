package libraryHomework;

public class Book {
    private String name;
    private int year;
    private double price;
    private Author author;
    private LibraryPosition libraryPosition;

    public Book(String name, int year, Author author, double price) {
        this.name = name;
        this.year = year;
        this.price = price;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }

    public Author getAuthor() {
        return author;
    }

    public LibraryPosition getLibraryPosition() {
        return libraryPosition;
    }

    public void setLibraryPosition(LibraryPosition libraryPosition) {
        this.libraryPosition = libraryPosition;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", author=" + author +
                ", libraryPosition=" + libraryPosition +
                '}';
    }
}
