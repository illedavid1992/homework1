package libraryHomework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Library {
    private List<Book> ListOfBooks = new ArrayList<Book>();

    public boolean addBook(Book book) {
        return ListOfBooks.add(book);
    }

    public boolean removeBook(Book book) {
        return ListOfBooks.remove(book);
    }

    public HashMap<Author, List<Book>> filterByAuthor(Author author) {
        HashMap<Author, List<Book>> filteredBooksHashMap = new HashMap<Author, List<Book>>();

        List<Book> filteredBooks = new ArrayList<Book>();
        for (Book book : ListOfBooks) {
            if(book.getAuthor().equals(author)){
                filteredBooks.add(book);
            }
        }
        filteredBooksHashMap.put(author, filteredBooks);

        return filteredBooksHashMap;
    }

    public void displayBooks(){
        for (Book book: ListOfBooks) {
            System.out.println("Book "+ book.getName() + "(" + book.getPrice() +"RON), by "+ book.getAuthor().getName() + ", published in " + book.getYear());
        }
    }

    public List<Book> searchBook(String name) {
        List<Book> filteredBooks = new ArrayList<Book>();
        for (Book book: ListOfBooks) {
            if(book.getName().equals(name)){
                filteredBooks.add(book);
            }
        }
        return filteredBooks;
    }

    public boolean updateBookPosition(Book book, LibraryPosition libraryPosition){
        boolean wasUpdated = false;
        for(Book libraryBook: ListOfBooks) {
            if(libraryBook.equals(book)){
                libraryBook.setLibraryPosition(libraryPosition);
                wasUpdated = true;
            }
        }
        return wasUpdated;
    }

}
