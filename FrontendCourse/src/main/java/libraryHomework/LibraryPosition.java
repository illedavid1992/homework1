package libraryHomework;

public class LibraryPosition {
    private String row;
    private String column;

    public LibraryPosition (String row, String column){
        this.row = row;
        this.column = column;
    }

    public String getRow() {
        return row;
    }

    public String getColumn() {
        return column;
    }
}
