package libraryHomework;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Author ionCreanga = new Author("Ion Creanga", "ion.creanga@gmail.com");
        Author mihaiEminescu = new Author("Mihai Eminescu", "mihai.eminescu@gmail.com");
        Author octavianPaler = new Author("Octavian Paler", "octavian.paler@gmail.com");

        Book drumuriPrinMemorie = new Book("Drumuri Prin Memorie",2000, octavianPaler, 50 );
        Book viataPeUnPeron = new Book ("Viata pe un Peron", 2002, octavianPaler, 100);
        Book luceafarul = new Book ("Luceafarul", 1850, mihaiEminescu, 100);
        Book somnoroasePasarele = new Book ("Somnoroase Pasarele", 1820, mihaiEminescu, 8);
        Book amintiriDinCopilarie = new Book ("Amintiri din copilarie", 1890,ionCreanga, 35);
        Book laScaldat = new Book ("La Scaldat", 1895,ionCreanga, 101);

        Library librarie = new Library();
        librarie.addBook(drumuriPrinMemorie);
        librarie.addBook(viataPeUnPeron);
        librarie.addBook(luceafarul);
        librarie.addBook(somnoroasePasarele);
        librarie.addBook(amintiriDinCopilarie);
        librarie.addBook(laScaldat);

        System.out.println("----------------------------------------START-------------------------------------");

        librarie.displayBooks();

        System.out.println("----------------------------------------------------------------------------------");

        HashMap<Author, List<Book>> filteredBooks = librarie.filterByAuthor(octavianPaler);
        for(List<Book> books : filteredBooks.values()){
            for(Book book : books){
                System.out.println("Book "+ book.getName() + "(" + book.getPrice() +"RON), by "+ book.getAuthor().getName() + ", published in " + book.getYear());
            }
        }

        System.out.println("----------------------------------------------------------------------------------");

        List<Book> books = librarie.searchBook("Somnoroase Pasarele");
        for(Book book : books){
            System.out.println(book.toString());
        }

        System.out.println("----------------------------------------------------------------------------------");

        librarie.removeBook(laScaldat);
        librarie.displayBooks();

        System.out.println("----------------------------------------------------------------------------------");

        LibraryPosition position = new LibraryPosition("rand 2", "raft 3");
        librarie.updateBookPosition(drumuriPrinMemorie, position);

        List<Book> carti = librarie.searchBook("Drumuri Prin Memorie");
        for(Book carte : carti){
            System.out.println(carte.getLibraryPosition().getRow() + " " + carte.getLibraryPosition().getColumn());
        }
        System.out.println("----------------------------------END---------------------------------------------");

    }
}
