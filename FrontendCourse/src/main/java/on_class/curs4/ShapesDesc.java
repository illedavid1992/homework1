package on_class.curs4;

public class ShapesDesc {
    public static void main(String[] args) {
//        Circle myCircle = new Circle(3f);
//        myCircle.setRadius(5);
//        Circle myCircle2 = new Circle(3f);
//
//        //Square
//        Square mySquare = new Square(4);
//        System.out.println("Square area:" + mySquare.getArea());
//        System.out.println("Square perimeter:" + mySquare.getPerimeter());
//        mySquare.displaySquare();

        //Rectangle
        Rectangle myRectangle = new Rectangle(4,2);
        System.out.println("Rectangle area:" +myRectangle.getArea());
        System.out.println("Rectangle perimeter:" +myRectangle.getPerimeter());
        System.out.println("Rectangle diagonal:" +myRectangle.getDiagonal());
    }
}
