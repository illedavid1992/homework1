package on_class.curs4;

public class Exercises {
    public static boolean even(int x) {
       return x % 2 == 0;
    }

    public static int suma(int a, int b){
        return a+b;
    }

    //Return sum of even numbers from a to b
    public static int evenSumOfRangeNumbers(int inc, int fin){
        int sum = 0;
        for(int i=inc; i<=fin; i++){
            if (even(i))
                sum = suma(sum, i);
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(evenSumOfRangeNumbers(2,4));
    }
}
