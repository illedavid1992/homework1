package on_class.curs4;

public class Rectangle {
    private double length, width;


    public Rectangle(double length, double width) {
        this.width = width;
        this.length = length;
    }

    public double getPerimeter() {
        return (length + width) * 2;
    }

    public double getArea() {
        return width * length;
    }

    public double getDiagonal() {
        return Math.sqrt(Math.pow(width, 2) + Math.pow(length, 2));
    }

}

