package on_class.curs4;

public class Square {
        private float side;

        public Square(float side) {
            this.side = side;
        }

        //setter
        public void setSide(float side) {
            this.side = side;
        }

        public float getPerimeter() {
            return side * 4;
        }

        public float getArea() {
            return side * side;
        }
        public void displaySquare(){
            System.out.println("This is a square with side:" + this.side);
        }
    }