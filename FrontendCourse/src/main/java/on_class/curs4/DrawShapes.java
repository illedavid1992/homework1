package on_class.curs4;

public class DrawShapes {

    public static void drawLine(int m){
        for (int i=1; i<=m; i++)
            System.out.print("*");
    }

    public static void drawFullShape(int n, int m){
        for (int i = 1; i<=n; i++){
            drawLine(m);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        drawFullShape(4, 4);
    }
}
