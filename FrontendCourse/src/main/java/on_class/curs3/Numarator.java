package on_class.curs3;

import java.util.Locale;

public class Numarator {

    public static void displayElementsByLetter(String letter){
        switch (letter.toUpperCase()) {
            case "A":
                System.out.println("apples");
                break;
            case "B":
                System.out.println("beer");
                break;
            case "C":
                System.out.println("cider");
                break;
            default:
                System.out.println("We only learned A, B, C");
        }

    }

    public static void main(String[] args) {
        int inc = Integer.parseInt(args[0]);
        int fin = Integer.parseInt(args[1]);
        int aux = inc;
        System.out.println("Count from:"+inc+" to:" +fin);

//structure with precondition
while(inc<=fin){
    System.out.println(inc);
    inc++;
}
//structure with post-condition
        inc = aux;
        System.out.println("Count with post-condition");
        do {
            System.out.println(inc);
            inc++;
        }while (inc<=fin);

        // on line condition - for
        inc = aux;
        System.out.println("Display values with FOR");
        for (int i=inc; i<=fin; i++){
            System.out.println(i);
        }

        int[] list = {1, 34, 56, 7};
        System.out.println("Valoarea elementelor din lista:");
        for(int i=0; i<= list.length-1; i++)
            System.out.println(list[i]);

        // Se recomanda!!!
        for (int val:list)
            System.out.println(val);

// use switch
        displayElementsByLetter("A");
        displayElementsByLetter("b");
        displayElementsByLetter("c");
        displayElementsByLetter("g");
    }
}
