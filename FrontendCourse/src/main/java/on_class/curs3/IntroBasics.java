package on_class.curs3;

public class IntroBasics {
    int varsta;


    public static void test(){
        System.out.println("Hello World!");
    }

    public static int isOnInterval(int x){
        //verify that x is on [0,100] and increment with 10
        if (x>=0 && x<=100) {
            System.out.println("x in interval [0,100]");
            x += 10;
        }else
            System.out.println("x not in [0,100]");
        return x;
    }

    public static int sum(int x, int y){
        return x+y;
    }

    public static void main(String[] args) {
        test();
        final double PI = 3.14;
        int nr1 = 8, nr2 = 3;
        System.out.println("Numar 1 are valoarea: " + nr1);
        nr1 = 10;
        System.out.println("Numar 1 are valoarea: " + nr1);

        boolean isFirst = false, isLast = true;
        char c='r';
        String myFirstName="David";
        String mySecondName="Ille";

        System.out.println(nr1 + 5);
        System.out.println("5" + nr1);

        System.out.println(myFirstName + " " +mySecondName);
        System.out.println("Lungime nume:"+(myFirstName+mySecondName).length());
        int prod = nr1 * nr2;
        int cat = nr1/nr2;
        int rest = nr1%nr2;
        float rez = (float) nr1/nr2;

        System.out.println("Prod:"+prod+" cat:"+cat+" rest:"+rest+" Impartire real:"+rez);

        //Pre-postIncrement vs Post-postIncrement
        int x=1;
        x=x+1; //x++ sau x+=1
        x+=5; //x=x+5

        //x++ vs ++x
        x=1;
        int postIncrement = (x++)+5;
        x=1;
        int preIncrement = (++x)+5;
        System.out.println("Increment :" +postIncrement+ " vs preincrement :" +preIncrement);

        // call methode
        System.out.println(isOnInterval(10));
        System.out.println(isOnInterval(101));
        //sum of 2 numbers
        System.out.println("Suma=" + sum(4, 6));
    }
}
