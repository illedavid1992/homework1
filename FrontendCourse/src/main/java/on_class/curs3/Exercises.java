package on_class.curs3;

import homework.SumOfNumbers;

public class Exercises {

    //get max from 2 elements
    public static int max(int a, int b) {
        if (a > b)
            return a;
        else
            return b;

    }

    //check odd numbers
    public static boolean odd(int x) {
        if (x % 2 != 0)
            return true;
        else return false;
    }

    public static void showOddNumbers(int x, int y) {
        for (int i = x; i <= y; i++)
            if (odd(i))
                System.out.println(i);
    }


    public static void main(String[] args) {
//        System.out.println(max(1, 100));
//        System.out.println(max(8, 8));
//        System.out.println(max(9, 0));
//        int x = Integer.parseInt(args[0]);
//        int y = Integer.parseInt(args[1]);
//        System.out.println("Display odd number from " + x + " to " + y);
//        showOddNumbers(x, y);
        SumOfNumbers.sumOfFirst100Numbers();
    }
}
