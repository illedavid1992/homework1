package homework;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

public class LeapYear {

    public static boolean isLeapYear(int i) {

        if ((i % 4 == 0 & i % 100 != 0) || (i % 100 == 0 & i % 400 != 0)) {
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
        if (x >= 1900 & x <= 2016) {
            if (isLeapYear(x)) {
                System.out.println("Month February has 29 days");
            } else {
                System.out.println("Month February has 28 days");
            }
        } else {
            System.out.println("The number entered is not in the expected interval");
        }
    }
}