package homework;

public class SumOfNumbers {

    public static int sumOfFirst100Numbers() {
        int sum=0;
        for (int i=1; i<=100; i++){
            sum=sum+i;
        }
        return sum;
    }
    public static void main(String[] args) {
        System.out.println("Sum of first 100 numbers is: "+ sumOfFirst100Numbers());
    }
}
