package homework;

public class PrimeNumbers {
    public static boolean isPrimeNumber(int x) {
        for (int i = 2; i <= x / 2; i++)
            if (x % i == 0)
                return false;
        return true;
    }


    public static void primeNumbers() {
        System.out.println(1);
        for (int i = 2; i < 1000000; i++) {
            if (isPrimeNumber(i)) {
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) {
        primeNumbers();
    }
}

