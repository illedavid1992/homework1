package calculatorHomework;

import calculator.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

@Test
public class Tests {
    public static Calculator c;

    @BeforeClass(groups = {"positive tests", "negative tests"})
    public void setUp() {
        System.out.println("Set up test elements");
        c = new Calculator();
    }

    @DataProvider(name = "sumDataProvider")
    public Object[][] sumDataProvider() {
        return new Object[][]{{2, 5, 7}, {3, 7, 10}, {4, 5, 9}};
    }

    @DataProvider(name = "invalidResultsDataProvider")
    public Object[][] invalidResultsDataProvider() {
        return new Object[][]{{2, 5, 17}, {3, 7, 101}, {4, 5, 19}};
    }

    @DataProvider(name = "decreaseResults")
    public Object[][] decreaseResults() {
        return new Object[][]{{2, 5, -3}, {7, 3, 4}, {4, 5, -1}};
    }

    @DataProvider(name = "negativeDecreaseResults")
    public Object[][] negativeDecreaseResults() {
        return new Object[][]{{2, 5, 3}, {7, 3, -4}, {4, 5, 1}};
    }

    @DataProvider(name = "multiplicationResults")
    public Object[][] multiplicationResults() {
        return new Object[][]{{2, 5, 10}, {7, 3, 21}, {4, 5, 20}};
    }

    @DataProvider(name = "negativeMultiplicationResults")
    public Object[][] negativeMultiplicationResults() {
        return new Object[][]{{2, 5, 20}, {7, 3, 22}, {4, 5, 19}};
    }

    @DataProvider(name = "divisionResults")
    public Object[][] divisionResults() {
        return new Object[][]{{10, 5, 2}, {7, 2, 3.5}, {40, 5, 8}};
    }

    @DataProvider(name = "negativeDivisionResults")
    public Object[][] negativeDivisionResults() {
        return new Object[][]{{2, 5, 20}, {7, 3, -22}, {4, -5, 19}};
    }

    @DataProvider(name = "squareRootResults")
    public Object[][] squareRootResults() {
        return new Object[][]{{89, 1, 9.4339}, {36, 1, 6}, {25, 1, 5}};
    }

    @DataProvider(name = "invalidSquareRootResults")
    public Object[][] invalidSquareRootResults() {
        return new Object[][]{{89, 1, 8.4339}, {987, 1, 6}, {55, 1, 5}};
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}, groups = {"negative tests"})
    public void operatorException() {
        c.compute(1, 1, "[");
    }

    @Test(dataProvider = "sumDataProvider", groups = {"positive tests"})
    public void sumOfNumbers(double a, double b, double result) {
        double suma = c.compute(a, b, "+");
        Assert.assertEquals(suma, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + suma);
    }

    @Test(dataProvider = "invalidResultsDataProvider", groups = {"negative tests"})
    public void invalidResultsForSum(double a, double b, double result) {
        double suma = c.compute(a, b, "+");
        Assert.assertNotEquals(suma, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + suma);
    }

    @Test(dataProvider = "decreaseResults", groups = {"positive tests"})
    public void decreaseNumbers(double a, double b, double result) {
        double dif = c.compute(a, b, "-");
        Assert.assertEquals(dif, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + dif);

    }

    @Test(dataProvider = "negativeDecreaseResults", groups = {"negative tests"})
    public void invalidResultsForDecrease(double a, double b, double result) {
        double dif = c.compute(a, b, "-");
        Assert.assertNotEquals(dif, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + dif);
    }

    @Test(dataProvider = "multiplicationResults", groups = {"positive tests"})
    public void multiplicateNumbers(double a, double b, double result) {
        double multi = c.compute(a, b, "*");
        Assert.assertEquals(multi, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + multi);

    }

    @Test(dataProvider = "negativeMultiplicationResults", groups = {"negative tests"})
    public void invalidResultsForMultiplication(double a, double b, double result) {
        double multi = c.compute(a, b, "*");
        Assert.assertNotEquals(multi, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + multi);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class}, groups = {"negative tests"})
    public void divisionException() {
        c.compute(8, 0, "/");
    }

    @Test(dataProvider = "divisionResults", groups = {"positive tests"})
    public void divOfNumbers(double a, double b, double result) {
        double div = c.compute(a, b, "/");
        Assert.assertEquals(div, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + div);
    }

    @Test(dataProvider = "negativeDivisionResults", groups = {"negative tests"})
    public void invalidResultsForDivision(double a, double b, double result) {
        double div = c.compute(a, b, "/");
        Assert.assertNotEquals(div, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + div);
    }

    @Test(dataProvider = "squareRootResults", groups = {"positive tests"})
    public void squareRoot(double a, double b, double result) {
        double root = c.compute(a, b, "SQRT");
        Assert.assertEquals(result, c.compute(a, b, "SQRT"), 4);
        System.out.println("Expected result is: " + result + " Actual result is: " + root);
    }

    @Test(dataProvider = "invalidSquareRootResults", groups = {"negative tests"})
    public void invalidSquareRoot(double a, double b, double result) {
        double root = c.compute(a, b, "SQRT");
        Assert.assertNotEquals(root, result);
        System.out.println("Expected result is: " + result + " Actual result is: " + root);
    }

    @AfterClass
    public void cleanUp() {
        System.out.println("Turn of the computer");
    }

}
